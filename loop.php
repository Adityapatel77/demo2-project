<!DOCTYPE html>
<html>
<head>
	<title>Loop</title>
</head>
<body>
<?php
$t = date('H');
if($t<12){
	echo "good morning";
}elseif($t<18){
	echo "good afternoon";
}else{
	echo "good evening";
}
echo "</br>";
?>

<?php
$x = "yellow";
switch ($x) {
	case 'red':
		echo "favourite color is red";
		break;
	case 'yellow':
		echo "favourite color is yellow";
		break;
	case 'blue':
		echo "favourite color is blue";
		break;
	
	default:
		echo "selected color is nither red, yellow or blue";
		break;
}
echo "</br>";
echo "</br>";
echo "print number using While loop";
echo "</br>";
$x = 0;
while($x <= 100){
	echo "the number is.$x"."</br>";
	$x+=10;
}
echo "</br>";
echo "</br>";
echo "print number using do while loop";
echo "</br>";
$x = 1;
do{
	echo "the number is. $x". "</br>";
	$x++;
}while($x <=5);
echo "</br>";
echo "</br>";
echo "print number using for loop";
echo "</br>";
for($x=0; $x<=10; $x++){
	echo "the number is .$x"."</br>";
}
echo "</br>";
echo "print number using foreach loop";
echo "</br>";
$x =  array('red', 'yellow','green','blue' );
foreach ($x as $value) {
	echo $value."</br>";
}
echo "</br>";
echo "print number using  break";
echo "</br>";
for($x=0; $x<=10; $x++){
	
	if($x == 3){
		break;
	}
	echo "the number is. $x"."</br>";
}
echo "</br>";
echo "print number using  continue";
echo "</br>";
for($x=0; $x<=10; $x++){
	
	if($x == 3){
		continue;
	}
	echo "the number is. $x"."</br>";
}

?>


</body>
</html>