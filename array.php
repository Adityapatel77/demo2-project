<!DOCTYPE html>
<html>
<head>
	<title>Array </title>
</head>
<body>
</body>
<p> Indexed Array</p>
<?php
$arr = array("Volvo","BMW","Saab");
echo "I like".$arr[0].",".$arr[1].",".$arr[2].",";
?>
<p> Associative Array </p>
<?php
$age=array("Peter"=>"35","Ben"=>"37","Joe"=>"43");

foreach($age as $x=>$x_value)
  {
  echo "Key=" . $x . ", Value=" . $x_value;
  echo "<br>";
  }
?>
<p> Mutlidimensional Array </p>
<?php
$cars=array
  (
  array("Volvo",100,96),
  array("BMW",60,59),
  array("Toyota",110,100)
  );
  
echo $cars[0][0].": Ordered: ".$cars[0][1].". Sold: ".$cars[0][2]."<br>";
echo $cars[1][0].": Ordered: ".$cars[1][1].". Sold: ".$cars[1][2]."<br>";
echo $cars[2][0].": Ordered: ".$cars[2][1].". Sold: ".$cars[2][2]."<br>";
?>
<p> Array Chunk </p>
<?php
$cars=array("Volvo","BMW","Toyota","Honda","Mercedes","Opel");
print_r(array_chunk($cars,2));
?>
<p> Array Combine </p>
<?php
$fname=array("Raj","Parth","harsh");
$age=array("35","37","43");

$c=array_combine($fname,$age);
print_r($c);
?>
<p> Array Difference </p>
<?php
$a1=array("a"=>"red","b"=>"green","c"=>"blue","d"=>"yellow");
$a2=array("e"=>"red","f"=>"green","g"=>"blue");

$result=array_diff($a1,$a2);
print_r($result);
?>
<p> array Filter </p>
<?php
function test($var){
	return($var & 1);
}
$a1 = array(1,3,2,5,4,6,7 );
print_r(array_filter($a1,"test"))
?>
<p> array Map </p>
<?php
function myfunction($v){
	return($v+$v);
}
$a = array(1,2,3,4,5,6);
print_r(array_map("myfunction", $a));
?>
<p>Array merge</p>
<?php
$a = array("a"=>"red","b"=>"green");
$b = array("c"=>"yellow","d"=>"pink");
print_r(array_merge($a,$b));
?>
<p>Array Multisport</p>
<?php
$a = array(1,10,20,30,40);
$b = array(4,5,6,7,8);
$num = array_merge($a,$b);
array_multisort($num,SORT_ASC,SORT_NUMERIC);
print_r($num);
?>
<p>Array reduce</p>
<?php
function myfunction1($a1,$a2){
	return $a1 . "-". $a2;
}
$a = array("dog","cat","horse");
print_r(array_reduce($a,"myfunction1",5));
?>
<p>Array Slice</p>
<?php
$a = array("red","green","yellow","brown","orange");
print_r(array_slice($a, 2,2,"true")); 
?>
<p>Compact function</p>
<?php
$firstname = "Aditya";
$lastname = "Patel";
$age = "23";
$result = compact("firstname","lastname","age");
print_r($result);
?>
<p>current function</p>
<?php
$people = array("Peter", "Joe", "Glenn", "Cleveland");

echo current($people) . "<br>"; 
echo next($people) . "<br>";
echo current($people) . "<br>";
echo prev($people) . "<br>"; 
echo end($people) . "<br>";
echo prev($people) . "<br>"; 
echo current($people) . "<br>"; 
echo reset($people) . "<br>"; // Moves the internal pointer to the first element of the array, which is Peter
echo next($people) . "<br>" . "<br>"; // The next element of Peter is Joe

print_r (each($people)); // Returns the key and value of the current element (now Joe), and moves the internal pointer forward
?>
<p>In array function for search</p>
<?php
$people = array("Peter", "Joe", "Glenn", "Cleveland", 23);

if (in_array("23", $people, TRUE))
  {
  echo "Match found<br>";
  }
else
  {
  echo "Match not found<br>";
  }
if (in_array("Glenn",$people, TRUE))
  {
  echo "Match found<br>";
  }
else
  {
  echo "Match not found<br>";
  }

if (in_array(23,$people, TRUE))
  {
  echo "Match found<br>";
  }
else
  {
  echo "Match not found<br>";
  }
?>
<p>List People</p>
<?php
$my_array = array("Dog","Cat","Horse");

list($a, $b, $c) = $my_array;
echo "I have several animals, a $a, a $b and a $c.";
?>

</body>
</html>