<!DOCTYPE html>
<html>
<head>
	<title>Jquery Animation</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	
</head>
<body>
<p>Click to toggle fade/in out button</p>
<br> 
<div id="div1" style="width: 80px;height: 80px; background-color: red"></div><br>
<div id="div2" style="width: 80px;height: 80px; background-color: green"></div><br>
<div id="div3" style="width: 80px;height: 80px; background-color: yellow"></div><br>
<div>
<button id="btn1">Fade in/out</button>
</div>
<br>

<br>
<button id="btn2">Start Animation</button><br>
<div style="background:#98bf21;height:100px;width:100px;position:absolute;margin-top: 10px" id="div4"></div><br>
</div>
</body>
</html>
<script>
$(document).ready(function(){
  $("#btn1").click(function(){
    $("#div1").fadeToggle();
    $("#div2").fadeToggle("slow");
    $("#div3").fadeToggle(3000);

  });
});
</script>
<script>
	$(document).ready(function(){
		$("#btn2").click(function(){
			var div = $("#div4");
			div.animate({height:'300px',opacity: '0.4'}, "slow");
			div.animate({width:'300px',opacity: '0.8'}, "slow");
			div.animate({height:'100px',opacity: '0.4'}, "slow");
			div.animate({width:'100px',opacity: '0.8'}, "slow");
		});
	});
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#demo3:button").click(function(){
			var div = $("#div4");
			div.animate({left: '100px'}, "slow");
			div.animate({fontSize: '4em'}, "slow");
		});
	});
</script>