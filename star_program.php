<!DOCTYPE html>
<html>
<head>
	<title>Logical program - star</title>
</head>
<body>
<p>Star Program</p>
<p><b>Pattern 1</b><p>
<?php
for ($i=1; $i <=5 ; $i++) { 
	for ($j=1; $j <=5 ; $j++) { 

		echo "* ";
	}
	echo "</br>";
}
?>

<p><b>Pattern 2</b><p>
<?php
for ($i=0; $i <= 5; $i++) { 
	for($j=1; $j<=$i; $j++){
		echo "* ";
	}
	echo "</br>";
}
?>
<br>
<p><b>Pattern 3 </b></p>
<?php
for($i=0; $i<=5; $i++){
	for($j= 5-$i; $j>=1; $j--){
		echo "* ";
	}
	echo "</br>";
}
?>
<p><b>Pattern 5 </b></p>
<?php
for($i=0;$i<=5;$i++)
{  
for($k=5;$k>=$i;$k--)
{  
echo "  ";  
}  
for($j=1;$j<=$i;$j++)
{  
echo "* ";  
}  
echo "<br>";  
}  
 for ($i=4; $i>=1 ; $i--) { 
 for($k=5;$k>=$i;$k--){  
echo "  ";  
}  
for($j=1;$j<=$i;$j++){  
echo "* ";  
}  
echo "<br>";  
 }
?>
<p><b>Pattern 6 </b></p>
<?php  
for($i=0;$i<=5;$i++)
{  
for($k=5;$k>=$i;$k--)
{  
echo "&nbsp;&nbsp;";  
}  
for($j=1;$j<=$i;$j++)
{  
echo "*  ";  
}  
echo "<br>";  
}  
for($i=4;$i>=1;$i--)
{  
for($k=5;$k>=$i;$k--)
{  
echo "&nbsp;&nbsp;";  
}  
for($j=1;$j<=$i;$j++)
{  
echo "*  ";  
}  
echo "<br>";  
}  
?>  
</body>
</html>