<!DOCTYPE html>
<html>
<head>
	<title>operator</title>
</head>
<body>
<?php
echo "Arithmetic operator.</br>";
echo "x = 10 </br>";
echo "y = 5";
echo "</br>";
echo "The addition of two number </br>";
$x = 10;
$y = 5;

echo $x + $y;
?>

<?php
echo "</br>";
echo "The modulus of two number </br>";
$x = 10;
$y = 5;

echo $x % $y;
?>
<?php
echo "</br>";
echo "The exponent of two number </br>";
$x = 10;
$y = 5;

echo $x ** $y;
?>
<?php
echo "</br>";
echo "</br>";
echo "Assignment Operator.</br>";
$x = 10;
$y = 5;
echo $x += $y;
echo "</br>";
echo "modulus of two operator in assignment </br>";
$x = 10;
$y = 5;
echo $x %= $y;
echo "</br>";
echo "multiplication of two operator in assignment </br>";
$x = 10;
$y = 5;
echo $x *= $y;
echo "</br>";
echo "</br>";
echo "comparison Operator.</br>";
$x = 10;
$y = 5;
echo var_dump($x == $y);
echo "</br>";
echo "identical operator </br>";
$x = 10;
$y = "10";
echo var_dump($x === $y);
echo "</br>";
echo "Spaceship operator. </br>";
$x = 5;  
$y = 10;

echo "x is less than y = ".($x <=> $y); 
echo "<br>";

$x = 10;  
$y = 10;

echo "x is equal to y = ".($x <=> $y); 
echo "<br>";

$x = 15;  
$y = 10;

echo "x is greater than y = ".($x <=> $y); 
echo "</br>";
echo "</br>";
echo "Logical Operator.</br>";
$x = 10;
$y = 5;
if ($x == 10 and $y == 5){
	echo "Hello World!";
}
echo "</br>";
echo "OR operator </br>";
$x = 10;
$y = 5;
if ($x == 10 or $y == 4){
	echo "Hello World! </br>";
}else{
	echo "something wrong </br>";
}

echo "XOR operator </br>";
$x = 10;
$y = 5;
if ($x == 10 xor $y == 5){
	echo "Hello World! </br>";
}else{
	echo "something wrong </br>";
}
 ?>
 <?php
 echo "</br>";
 echo "concatenation operator </br>";
$txt1 = "Hello";
$txt2 = " world!";
$txt1 .= $txt2;
echo $txt1;
echo "</br>";
echo "array Operator </br>";

$x = array("a" => "red", "b" => "green");  
$y = array("c" => "blue", "d" => "yellow");  

print_r($x + $y);
echo "</br>";
echo "equal array operator </br>";
$x =  array('a' => 'red' , 'b' => 'green');
$y =  array('c' => 'blue' , 'b' => 'yellow');
echo var_dump($x == $y);
echo "</br>";
echo "identity array operator </br>";
$x =  array('a' => 'red' , 'b' => 'green');
$y =  array('c' => 'blue' , 'b' => 'yellow');
echo var_dump($x === $y);
echo "</br>";
echo "Not equal array operator </br>";
$x =  array('a' => 'red' , 'b' => 'green');
$y =  array('c' => 'blue' , 'b' => 'yellow');
echo var_dump($x != $y);
echo "</br>";
echo "Ternary operator </br>";
echo $status = (empty($user)) ? "anonymous" : "logged in";
echo "</br>";
echo "when user is given </br>";
$user = "Aditya patel";
echo $status = (empty($user)) ? "anonymous" : "logged in";
echo "</br>";
echo "Null Coalescing </br>";
 echo $user = $_GET["user"] ?? "anonymous";
   echo("<br>");
  
   // variable $color is "red" if $color does not exist or is null
   echo $color = $color ?? "red";
?> 
</body>
</html>