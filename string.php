<!DOCTYPE html>
<html>
<head>
	<title>String Manipulation</title>
</head>
<body>
<?php 
echo "split string </br>";
$x = "Hello world!";
echo chunk_split($x,1,".");
echo "</br>";
echo "</br>";
echo "String to Array Conversion.</br>";
$x = "Hey! How r u ?";
print_r (explode(" ", $x));
echo "</br>";
echo "</br>";
echo "Array to string Conversion.</br>";
$x =  array("hello","how","r","u" );;
echo (implode(" ", $x));
?>
<p>Search the string "Hello good world!", find the value "WORLD" and replace it with "Peter":</p>
<?php
echo str_ireplace("WORLD","Peter","Hello good world!");
?>
<p> string replace using string replace </p>
<?php
$x = "hello world";
echo "the string is . $x"."</br>";
echo str_replace("world", "John", $x);
echo "</br>";
?>
<p> Encode and decode String </p>
<?php
echo str_rot13("hey how r u?")."</br>";
echo str_rot13("url ubj e h?");
?>
<p> String Split</p>
<?php
print_r(str_split("hello world", 2));
?>
<p> String Comparison </p>
<p> if function return 0, then string are equal.</p>
<?php
echo strcmp("Hello world!","Hello world!")."</br>";
echo strcmp("Hello wor!","Hello world!")."</br>";
echo strcmp("Hello world!","Hello word!")."</br>";
?>
<p> String length using strlen </p>
<?php
echo "The string is hello World </br>";
echo strlen("Hello World");
?>
<p> String Position </p>
<?php
echo strpos("hello world", "world");
?>
<p> String reverse </p>
<?php
echo strrev("Hello World");
?>
<p>Trim word from string</p>
<?php
$str = "Hello World!";
echo $str . "<br>";
echo trim($str,"Hed!");
?>
<p> String  Wordwrap </p>
<?php
$str = "An example of a long word is: Supercalifragulistic";
echo wordwrap($str,15,"<br>\n");
?> 
</body>
</html>